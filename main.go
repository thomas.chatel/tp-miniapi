package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func rootHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		fmt.Fprintf(w, time.Now().Format("15h04"))
	}
}

func helloHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodPost:
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}

		var author string
		if req.PostForm.Has("author") {
			author = req.PostForm.Get("author")
		} else {
			fmt.Fprintf(w, "Veuillez renseigner un auteur")
			return
		}

		var entry string
		if req.PostForm.Has("entry") {
			entry = req.PostForm.Get("entry")
			file, err := os.OpenFile("entries.txt", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0600)
			defer file.Close() // on ferme automatiquement à la fin de notre programme

			if err != nil {
				panic(err)
			}

			_, err = file.WriteString(entry + "\n") // écrire dans le fichier
			if err != nil {
				panic(err)
			}
		} else {
			fmt.Fprintf(w, "Veuillez renseigner un message")
			return
		}
		fmt.Fprintf(w, author + ":" + entry)
	}
}


func entriesHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		data, err := ioutil.ReadFile("entries.txt") // lire le fichier
		if err != nil {
			fmt.Fprintf(w, "no entries found")
		}
		fmt.Fprintf(w, string(data))
	}
}

func main() {
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/hello", helloHandler)
	http.HandleFunc("/entries", entriesHandler)
	http.ListenAndServe(":4567", nil)
}
